const express = require('express');

const router = express.Router();

// import user controller
const userControllers = require("../controllers/userControllers");

const auth = require('../auth')

const {verify, verifyAdmin} = auth;
//Routes

// User Registration

router.post('/', userControllers.registerUser);

//Get all users
router.get('/', userControllers.getAllUsers);

//Login
router.post('/login', userControllers.loginUser);

// Retrieving User Details
router.get('/getUserDetails', verify, userControllers.getUserDetails)

// checking if email does exist
router.post('/checkEmailExists', userControllers.checkEmailExists);

/*MINI ACTIVITY*/
//updating a regular user to admin
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

//updating details of the user
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

//Enroll registered user
router.post('/enroll', verify, userControllers.enroll);

// Get the enrolled subjects of a user

router.get('/getEnrollments', verify, userControllers.getEnrollments)

module.exports = router;	