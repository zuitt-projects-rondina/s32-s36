const express = require('express');
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");

//object destructuring
const {verify, verifyAdmin} = auth;

//Creating course 
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// Retrieving all courses

router.get('/', courseControllers.getAllCourses)

// Retrieve a single course
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse)

// Archiving document to false

router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse)

// activating course 
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

// retrieve all active course
router.get('/getActiveCourses', courseControllers.getActiveCourses);

// update a course

router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

// retrieving inactive course
router.get('/getInactiveCourses', verify, verifyAdmin, courseControllers.getInactiveCourses);

// find courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName);

// find courses by prices
router.post('/findCoursesByPrice', courseControllers.findCoursesByPrice)

//Enrollee's list by course

router.get('/getEnrollees/:id', verify, verifyAdmin, courseControllers.getEnrollees)

module.exports = router;
