const express = require('express');
const mongoose = require('mongoose');

//allows our backend app to be available to frontend
//cors - cross origin resource sharing
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');
const port = 4000;

const app = express();

mongoose.connect("mongodb+srv://admin_rondina:admin169@rondina-b169.sl6hj.mongodb.net/bookingAPI169?retryWrites=true&w=majority", {

			useNewUrlParser : true,
			useUnifiedTopology : true

});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));


//middleware
app.use(express.json());
app.use(cors());


app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))